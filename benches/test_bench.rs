use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("fib 48", |b| b.iter(|| fib::iterative(black_box(48))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);