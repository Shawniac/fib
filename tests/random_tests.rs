use fib;

mod common;

#[test]
fn test_zero_to_thirty_two_calculate() {
    for i in 0..=32 {
        assert_eq!(common::fib(i), fib::calculate(i.try_into().unwrap()).round() as u32);
    }
}

#[test]
fn test_zero_to_thirty_two_iterative() {
    for i in 0..=32 {
        assert_eq!(common::fib(i), fib::iterative(i.try_into().unwrap()) as u32);
    }
}

#[test]
fn test_zero_to_thirty_two_recursive() {
    for i in 0..=32 {
        assert_eq!(common::fib(i), fib::recursive(i.try_into().unwrap()) as u32);
    }
}