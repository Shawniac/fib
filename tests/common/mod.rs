pub fn fib(n: usize) -> u32 {
    // csv taken from: https://blog.abelotech.com/posts/first-500-fibonacci-numbers/
    let fib_nums = [
        0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765,
        10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269,
        2178309,
    ];

    let fib_result = fib_nums.get(n);

    match fib_result {
        None => panic!(
            "unsupported index {}, supporting {}..{}",
            n,
            0,
            fib_nums.len()-1
        ),
        Some(fib) => *fib,
    }
}