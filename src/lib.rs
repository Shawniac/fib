pub fn recursive(n: u32) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        n => recursive(n-1) + recursive(n-2),
    }
}

pub fn iterative(n: u64) -> u128 {
    let mut a = 0;
    let mut b = 1;

    match n {
        0 => 0,
        _ => {
            for _ in 0..n-1 {
                let c = a + b;
                a = b;
                b = c;
            }
            b
        }
    }
}

pub fn calculate(n: u64) -> f64 {
    let phi = (1.0 + (5 as f64).sqrt()) / 2.0;

    phi.powi(n as i32) / (5 as f64).sqrt()
}