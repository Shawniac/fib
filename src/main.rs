fn print_usage() {
    println!("");
    println!("A simple program returning the n-th fibonacci number");

    let app_name = std::env::args().nth(0).unwrap();
    println!("Usage: {} <n>", app_name);
}

fn main() {
    // parse commandline arguments
    let arg_num: String = match std::env::args().nth(1) {
        None => "".to_string(),
        Some(arg) => arg,
    };
    if arg_num.is_empty() {
        println!("ERROR: no input given");
        print_usage();

        return;
    } else if std::env::args().len() != 2 {
        println!("ERROR: there can only be exactly one argument");
        print_usage();

        return;
    }

    // parse n
    let n: u64 = match arg_num.trim().parse() {
        Ok(n) => n,
        Err(_) => {
            println!("please input a valid integer!");
            print_usage();

            return;
        }
    };

    // print all the fibonacci numbers up to and including n
    //for i in 0..=n {
    //    println!("fib({})={}", i, fib::iterative(i));
    //}

    // print the n-th fibonnaci number
    println!("fib({})={}", n, fib::iterative(n));
}
